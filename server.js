require("dotenv").config();
const cors = require("cors");
const express = require("express");

const app = express();
const PORT = process.env.PORT || 3333;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const moviesRoutes = require("./src/routes/movies.routes");
const CastRoutes = require("./src/routes/casts.routes");

app.use("/campaign/movies", moviesRoutes);
app.use("/campaign/casts", CastRoutes);

app.all("*", (req, res) => {
  res.send("Are you lost...!?");
});

app.listen(PORT, async () => {
  console.log(`Server up on http://localhost:${PORT}`);
});

module.exports = app;
