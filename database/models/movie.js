'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    static associate({ movieCast }) {
      // define association here
      this.hasMany(movieCast, { foreignKey: 'movie_id' });
    }
  };
  Movie.init({
    name: DataTypes.STRING,
    language: DataTypes.STRING,
    status: DataTypes.STRING,
    rating: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Movie',
  });
  return Movie;
};