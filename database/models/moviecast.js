'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movieCast extends Model {
    static associate({ Movie,  Cast}) {
      // define association here
      this.belongsTo(Cast, { foreignKey: 'cast_id' });
      this.belongsTo(Movie, { foreignKey: 'movie_id' });
    }
  };
  movieCast.init({
    movie_id: DataTypes.BIGINT,
    cast_id: DataTypes.BIGINT
  }, {
    sequelize,
    modelName: 'movieCast',
  });
  return movieCast;
};