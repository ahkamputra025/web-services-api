'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cast extends Model {
    static associate({movieCast}) {
      // define association here
      this.hasMany(movieCast, { foreignKey: 'cast_id' });
    }
  };
  Cast.init({
    name: DataTypes.STRING,
    birthday: DataTypes.DATE,
    deadday: DataTypes.DATE,
    rating: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Cast',
  });
  return Cast;
};