'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('movieCasts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      movie_id: {
        type: Sequelize.BIGINT,
        references: { model: 'movies', key: 'id' }
      },
      cast_id: {
        type: Sequelize.BIGINT,
        references: { model: 'casts', key: 'id' }
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('movieCasts');
  }
};