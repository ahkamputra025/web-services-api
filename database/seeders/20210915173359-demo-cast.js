'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('casts', [
      {
        name: 'Ryan Reynolds',
        birthday: '1976-10-23',
        deadday: null,
        rating: 3,
      },
      {
        name: 'Leonardo DiCaprio',
        birthday: '1974-11-11',
        deadday: null,
        rating: 2,
      },
      {
        name: 'Daniel Craig',
        birthday: '1968-03-02',
        deadday: null,
        rating: 1,
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('casts', null, {});
     */
  }
};
