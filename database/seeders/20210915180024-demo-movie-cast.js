'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('movieCasts', [
      {
        movie_id: 1,
        cast_id: 1,
      },
      {
        movie_id: 2,
        cast_id: 3,
      },
      {
        movie_id: 3,
        cast_id: 3,
      },
      {
        movie_id: 4,
        cast_id: 1,
      },
      {
        movie_id: 5,
        cast_id: 2,
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('movieCasts', null, {});
     */
  }
};
