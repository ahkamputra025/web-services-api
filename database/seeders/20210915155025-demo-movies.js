'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('movies', 
    [
      {
        name: 'Free Guy',
        language: 'english',
        status: 'started',
        rating: 1,
      },
      {
        name: 'No Time to Die',
        language: 'english',
        status: 'ongoing',
        rating: 2,
      },
      {
        name: 'Knives Out',
        language: 'english',
        status: 'ended',
        rating: 3,
      },
      {
        name: 'Deadpool',
        language: 'english',
        status: 'started',
        rating: 4,
      },
      {
        name: 'Inception',
        language: 'english',
        status: 'started',
        rating: 5,
      }
      
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('movies', null, {});
     */
  }
};
