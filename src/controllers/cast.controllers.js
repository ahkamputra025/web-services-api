const { Cast, Movie, movieCast } = require("../../database/models");
const method = {};

method.getCast = async (req, res) => {
  try {
    const dtCasts = await Cast.findAll({
      include: {
        model: movieCast,
        include: [{ model: Movie }],
      },
    });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Get Casts Successfully",
      data: dtCasts,
    });
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      statusText: "Internal Server Error",
      message: "Get Casts Failed",
    });
  }
};

module.exports = method;
