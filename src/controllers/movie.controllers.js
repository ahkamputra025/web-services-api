const { Movie, Cast, movieCast } = require("../../database/models");
const method = {};

method.getMovies = async (req, res) => {
  try {
    const dtMovies = await Movie.findAll({
      include: {
        model: movieCast,
        include: [{ model: Cast }],
      },
    });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Get Movies Successfully",
      data: dtMovies,
    });
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      statusText: "Internal Server Error",
      message: "Get Movies Failed",
    });
  }
};

module.exports = method;
