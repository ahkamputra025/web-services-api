const router = require("express").Router();
const { getCast } = require("../controllers/cast.controllers");

router.get("/", getCast);

module.exports = router;
