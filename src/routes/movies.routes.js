const router = require("express").Router();

const { getMovies } = require("../controllers/movie.controllers");

router.get("/", getMovies);

module.exports = router;
