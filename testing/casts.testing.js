const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = require("chai").expect;
const app = require("../server");

chai.use(chaiHttp);
chai.should();

describe("GET Casts API", () => {
  it("GET /campaign/casts --> API", (done) => {
    chai
      .request(app)
      .get("/campaign/casts")
      .end((err, res) => {
        res.body.should.be.a("object");
        expect(res.body.statusCode).to.equal(200);
        expect(res.body.statusText).to.equal("success");
        expect(res.body.message).to.equal("Get Casts Successfully");

        res.body.data.should.be.a("array");
        expect(res.body.data[0]).to.have.property("id");
        expect(res.body.data[0]).to.have.property("name");
        expect(res.body.data[0]).to.have.property("birthday");
        expect(res.body.data[0]).to.have.property("deadday");
        expect(res.body.data[0]).to.have.property("rating");
        expect(res.body.data[0]).to.have.property("movieCasts");

        res.body.data[0].movieCasts.should.be.a("array");
        expect(res.body.data[0].movieCasts[0]).to.have.property("id");
        expect(res.body.data[0].movieCasts[0]).to.have.property("movie_id");
        expect(res.body.data[0].movieCasts[0]).to.have.property("cast_id");
        expect(res.body.data[0].movieCasts[0]).to.have.property("Movie");

        res.body.data[0].movieCasts[0].Movie.should.be.a("object");
        expect(res.body.data[0].movieCasts[0].Movie).to.have.property("id");
        expect(res.body.data[0].movieCasts[0].Movie).to.have.property("name");
        expect(res.body.data[0].movieCasts[0].Movie).to.have.property(
          "language"
        );
        expect(res.body.data[0].movieCasts[0].Movie).to.have.property("status");
        expect(res.body.data[0].movieCasts[0].Movie).to.have.property("rating");

        done();
      });
  });
});
