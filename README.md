# Web services API

Campaign Junior Back-End (BE) Developer Test

# Getting started

1. Copy the .envExample file and create a .env file and add the connection DB and other and SIGNATURE (can be any word)
2. Install sequelize-cli how dev-dependency and execute this `sequelize db:create` next `sequelize db:migrate`
3. Install node-modules `$ npm i` and run with command `$ npm start`
4. Install mocha, chai and chai-http how dev-dependency and execute this `$ npm test`

# API List

service ecommerce API

| Routes | EndPoint           | Description                                        |
| ------ | ------------------ | -------------------------------------------------- |
| GET    | /campaign/movies   | Data form movies relationship with cast            |
| GET    | /campaign/casts    | Data form cast relationship with movies            |

```
GET
/campaign/movies
{
    "statusCode": 200,
    "statusText": "success",
    "message": "Get Movies Successfully",
    "data": [
        {
            "id": 1,
            "name": "Free Guy",
            "language": "english",
            "status": "started",
            "rating": 1,
            "movieCasts": [
                {
                    "id": 1,
                    "movie_id": 1,
                    "cast_id": 1,
                    "Cast": {
                        "id": 1,
                        "name": "Ryan Reynolds",
                        "birthday": "1976-10-23T00:00:00.000Z",
                        "deadday": null,
                        "rating": 3
                    }
                }
            ]
        },
        {
            "id": 2,
            "name": "No Time to Die",
            "language": "english",
            "status": "ongoing",
            "rating": 2,
            "movieCasts": [
                {
                    "id": 2,
                    "movie_id": 2,
                    "cast_id": 3,
                    "Cast": {
                        "id": 3,
                        "name": "Daniel Craig",
                        "birthday": "1968-03-02T00:00:00.000Z",
                        "deadday": null,
                        "rating": 1
                    }
                }
            ]
        },
        {
            "id": 3,
            "name": "Knives Out",
            "language": "english",
            "status": "ended",
            "rating": 3,
            "movieCasts": [
                {
                    "id": 3,
                    "movie_id": 3,
                    "cast_id": 3,
                    "Cast": {
                        "id": 3,
                        "name": "Daniel Craig",
                        "birthday": "1968-03-02T00:00:00.000Z",
                        "deadday": null,
                        "rating": 1
                    }
                }
            ]
        },
        {
            "id": 4,
            "name": "Deadpool",
            "language": "english",
            "status": "started",
            "rating": 4,
            "movieCasts": [
                {
                    "id": 4,
                    "movie_id": 4,
                    "cast_id": 1,
                    "Cast": {
                        "id": 1,
                        "name": "Ryan Reynolds",
                        "birthday": "1976-10-23T00:00:00.000Z",
                        "deadday": null,
                        "rating": 3
                    }
                }
            ]
        },
        {
            "id": 5,
            "name": "Inception",
            "language": "english",
            "status": "started",
            "rating": 5,
            "movieCasts": [
                {
                    "id": 5,
                    "movie_id": 5,
                    "cast_id": 2,
                    "Cast": {
                        "id": 2,
                        "name": "Leonardo DiCaprio",
                        "birthday": "1974-11-11T00:00:00.000Z",
                        "deadday": null,
                        "rating": 2
                    }
                },
                {
                    "id": 6,
                    "movie_id": 5,
                    "cast_id": 4,
                    "Cast": {
                        "id": 4,
                        "name": "Cillian Murphy",
                        "birthday": "1975-05-25T00:00:00.000Z",
                        "deadday": null,
                        "rating": 4
                    }
                }
            ]
        }
    ]
}

GET
/campaign/casts
{
    "statusCode": 200,
    "statusText": "success",
    "message": "Get Casts Successfully",
    "data": [
        {
            "id": 1,
            "name": "Ryan Reynolds",
            "birthday": "1976-10-23T00:00:00.000Z",
            "deadday": null,
            "rating": 3,
            "movieCasts": [
                {
                    "id": 1,
                    "movie_id": 1,
                    "cast_id": 1,
                    "Movie": {
                        "id": 1,
                        "name": "Free Guy",
                        "language": "english",
                        "status": "started",
                        "rating": 1
                    }
                },
                {
                    "id": 4,
                    "movie_id": 4,
                    "cast_id": 1,
                    "Movie": {
                        "id": 4,
                        "name": "Deadpool",
                        "language": "english",
                        "status": "started",
                        "rating": 4
                    }
                }
            ]
        },
        {
            "id": 3,
            "name": "Daniel Craig",
            "birthday": "1968-03-02T00:00:00.000Z",
            "deadday": null,
            "rating": 1,
            "movieCasts": [
                {
                    "id": 2,
                    "movie_id": 2,
                    "cast_id": 3,
                    "Movie": {
                        "id": 2,
                        "name": "No Time to Die",
                        "language": "english",
                        "status": "ongoing",
                        "rating": 2
                    }
                },
                {
                    "id": 3,
                    "movie_id": 3,
                    "cast_id": 3,
                    "Movie": {
                        "id": 3,
                        "name": "Knives Out",
                        "language": "english",
                        "status": "ended",
                        "rating": 3
                    }
                }
            ]
        },
        {
            "id": 2,
            "name": "Leonardo DiCaprio",
            "birthday": "1974-11-11T00:00:00.000Z",
            "deadday": null,
            "rating": 2,
            "movieCasts": [
                {
                    "id": 5,
                    "movie_id": 5,
                    "cast_id": 2,
                    "Movie": {
                        "id": 5,
                        "name": "Inception",
                        "language": "english",
                        "status": "started",
                        "rating": 5
                    }
                }
            ]
        },
        {
            "id": 4,
            "name": "Cillian Murphy",
            "birthday": "1975-05-25T00:00:00.000Z",
            "deadday": null,
            "rating": 4,
            "movieCasts": [
                {
                    "id": 6,
                    "movie_id": 5,
                    "cast_id": 4,
                    "Movie": {
                        "id": 5,
                        "name": "Inception",
                        "language": "english",
                        "status": "started",
                        "rating": 5
                    }
                }
            ]
        }
    ]
}
```
